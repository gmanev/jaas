The overal aim is to define a transformation of a subset of the Java VM instructions set along with the full Java VM semantics onto WebAssembly code. 

1. Get acquainted with the state of the art of Java to native compilers, especaially those being LLVM frontends. Collect past/existing project links and their status, achievements, blocking points.

    Known projects related to the subject:
    - [Cheerpj](https://www.leaningtech.com/pages/cheerpj.html)

2. What does WebAssembly (Wasm) instruction set offer? Types, memory access, control flow?
 Resources:
 - https://webassembly.github.io/spec/core/

3. Figure out if we can represent the semantics and a subset of Java VM instruction set of a Java program as a valid Wasm program? How/can we map Java types, exceptions, objects, memory management in Wasm?
 Resources:
 - https://docs.oracle.com/javase/specs/jvms/se15/jvms15.pdf

4. Provide a PoC asap