# JaaS

This project defines a subset of the Java VM suitable for building and deploying modern FaaS applications leveraging the existing Java ecosystem. 

The aim is to offer an alternative perspective for modern fine-granular service-based event-driven Java applications to be built and run efficiently. Most existing approaches we are aware of, strive to transform JVM code into native one depending on a full JRE environment.

This project explores another approach, limit the JVM to the extent code implements only the business logic and relies on external providers for the communication with external systems. The anticipated effect for the Java, Scala, Kotlin, etc. developers is that they will become more productive as they will not write a lot of plumbing code as they do today. The applications written in that Java VM subset would be capable to run on a more efficient FaaS runtime platform that is not built on "traditional" containers wrapping a JRE inside.